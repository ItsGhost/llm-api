use std::convert::Infallible;
use std::sync::{Arc, Mutex};

use flume::Sender;
use llm::{
    InferenceError, InferenceFeedback, InferenceParameters, InferenceStats, Model, OutputRequest,
    Prompt, TokenUtf8Buffer,
};
use rocket::response::stream::Event;
use rocket::tokio::task::JoinHandle;

use crate::inference::completion::{CompletionRequest, CompletionResponse, StopSequence};

use super::error::InferenceRequestError;

pub type ModelGuard = Arc<Mutex<Box<dyn Model>>>;

pub struct InferenceThreadRequest {
    pub token_sender: Sender<Event>,

    pub model_guard: ModelGuard,
    pub completion_request: CompletionRequest,
}

impl InferenceThreadRequest {
    pub fn is_aborted(&self) -> bool {
        self.token_sender.is_disconnected()
    }

    pub fn send_comment(&self, message: &str) {
        self.token_sender
            .send(Event::comment(message.to_string()))
            .unwrap();
    }

    pub fn send_event(&self, event_name: &str) {
        self.token_sender
            .send(Event::data("").event(event_name.to_string()))
            .unwrap();
    }

    pub fn send_done(&self) {
        if self.token_sender.is_disconnected() {
            return;
        }

        self.token_sender.send(Event::data("[DONE]")).unwrap();
    }

    pub fn send_error(&self, error: String) {
        println!("{}", error);
        self.token_sender
            .send(Event::data(error).event("error"))
            .unwrap();
        self.send_done();
    }
}

fn get_inference_params(completion_request: &CompletionRequest) -> InferenceParameters {
    let n_threads = crate::utils::get_n_threads();

    InferenceParameters {
        n_threads,
        n_batch: n_threads,
        sampler: Arc::new(completion_request.to_top_p_top_k()),
    }
}

// Perhaps might be better to clone the model for each thread...
pub fn start(req: InferenceThreadRequest) -> JoinHandle<Result<(), InferenceRequestError>> {
    info!("Starting inference ...");

    spawn_inference_thread(req)
}

fn spawn_inference_thread(
    req: InferenceThreadRequest,
) -> JoinHandle<Result<(), InferenceRequestError>> {
    info!("Spawning inference thread...");
    let handle = rocket::tokio::task::spawn_blocking(move || {
        let mut rng = req.completion_request.get_rng();

        let maximum_token_count = req.completion_request.get_max_tokens();

        let mut token_utf8_buf = TokenUtf8Buffer::new();
        let guard = req.model_guard.lock();

        let model = match guard.as_ref() {
            Ok(m) => m,
            Err(e) => {
                error!("Model locked, cannot be loaded");
                return Err(InferenceRequestError::ModelBusy);
            }
        };

        let mut session = model.start_session(Default::default());

        let mut output_request = OutputRequest::default();

        let inference_params = get_inference_params(&req.completion_request);

        let mut stats = InferenceStats::default();
        let start_at = std::time::SystemTime::now();
        let stop_sequences = match req.completion_request.stop.clone().unwrap() {
            StopSequence::Single(stop) => vec![stop],
            StopSequence::Multiple(stop) => stop,
        }
        .into_iter()
        .map(|s| s.trim().to_string());
        let mut buf = String::new();

        info!("Feeding prompt ...");
        req.send_comment("FEEDING_PROMPT");

        match session.feed_prompt::<Infallible, Prompt>(
            model.as_ref(),
            &inference_params,
            req.completion_request.prompt.as_str().into(),
            &mut output_request,
            |t| {
                if req.is_aborted() {
                    return Ok(InferenceFeedback::Halt);
                }

                if let Some(token) = token_utf8_buf.push(t) {
                    req.send_comment(format!("Processing token: {:?}", token).as_str());
                }

                Ok(InferenceFeedback::Continue)
            },
        ) {
            Ok(_) => {
                stats.feed_prompt_duration = start_at.elapsed().unwrap();

                info!(
                    "Done feeding prompt ... in {:?}",
                    stats.feed_prompt_duration
                );
            }
            Err(e) => {
                req.send_error(e.to_string());
                return Err(e.into());
            }
        };

        req.send_comment("Generating tokens ...");
        req.send_comment("GENERATING_TOKENS");

        // Reset the utf8 buf
        token_utf8_buf = TokenUtf8Buffer::new();

        let mut stop_handler = req.completion_request.get_stop_handler(model.as_ref());

        let mut tokens_processed = 0;

        while tokens_processed < maximum_token_count {
            if req.is_aborted() {
                break;
            }

            let token = match session.infer_next_token(
                model.as_ref(),
                &inference_params,
                &mut output_request,
                &mut rng,
            ) {
                Ok(t) => t,
                Err(InferenceError::EndOfText) => {
                    break;
                }
                Err(e) => {
                    req.send_error(e.to_string());
                    break;
                }
            };

            if stop_handler.check(&token) {
                break;
            }

            // Buffer the token until it's valid UTF-8, then call the callback.
            if let Some(tokens) = token_utf8_buf.push(&token) {
                match req.token_sender.send(CompletionResponse::to_event(tokens)) {
                    Ok(_) => {}
                    Err(_) => {
                        break;
                    }
                }
            }

            tokens_processed += 1;
        }

        stats.predict_duration = start_at.elapsed().unwrap();
        stats.predict_tokens = tokens_processed;

        info!("Inference stats: {:?}", stats);

        if !req.token_sender.is_disconnected() {
            req.send_done();
        }

        Ok(())

        // Run inference
        // let res = session.infer::<Infallible>(
        //     model.as_ref(),
        //     &mut rng,
        //     &InferenceRequest {
        //         prompt,
        //         play_back_previous_tokens: false,
        //         // maximum_token_count: Some(8472),
        //         parameters: Some(&InferenceParameters {
        //             // n_batch: 4,
        //             // n_threads: 2,
        //             ..Default::default()
        //         }),
        //         ..Default::default()
        //     },
        //     // OutputRequest
        //     &mut Default::default(),
        //     |r| match r {
        //         InferenceResponse::InferredToken(t) => {
        //             tx_token.try_send(get_completion_resp(t)).unwrap();
        //             // for ch in t.chars() {
        //             //     // for each character in t, send a completion response
        //             //     tx.try_send(get_completion_resp(ch.to_string())).unwrap();
        //             // }

        //             Ok(if *Arc::clone(&req.abort_token).read() {
        //                 InferenceFeedback::Halt
        //             } else {
        //                 InferenceFeedback::Continue
        //             })
        //         }
        //         _ => Ok(if *Arc::clone(&req.abort_token).read() {
        //             InferenceFeedback::Halt
        //         } else {
        //             InferenceFeedback::Continue
        //         }),
        //     },
        // );

        // match res {
        //     Ok(result) => {
        //         println!(
        //             "\n\n===\n\nInference stats:\n\n{}\naborted: {}\n",
        //             result,
        //             *Arc::clone(&req.abort_token).read()
        //         );
        //     }
        //     Err(err) => {
        //         tx_token
        //             .try_send(get_completion_resp(err.to_string()))
        //             .unwrap();
        //     }
        // }
    });
    handle
}
