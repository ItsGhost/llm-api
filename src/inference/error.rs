use std::fmt::{Display, Formatter, Pointer};

use llm::InferenceError;

#[derive(Debug)]
pub enum InferenceRequestError {
    ModelBusy,
    InferenceError(String),
}

impl Display for InferenceRequestError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:?}", self))
    }
}

impl std::error::Error for InferenceRequestError {}

impl From<InferenceError> for InferenceRequestError {
    fn from(value: InferenceError) -> Self {
        Self::InferenceError(value.to_string())
    }
}
