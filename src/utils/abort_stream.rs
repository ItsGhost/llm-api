use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};

use flume::Receiver;
use rocket::futures::Stream;
use rocket::http::hyper::body::Bytes;
use rocket::tokio::sync::RwLock;
use rocket::tokio::task::JoinHandle;
use rocket::Error;

pub struct AbortStream {
    pub stream: Pin<Box<dyn Stream<Item = Bytes> + Send>>,
    pub abort_flag: Arc<RwLock<bool>>,
    pub thread: JoinHandle<()>,
}

impl AbortStream {
    pub fn new(
        receiver: Receiver<Bytes>,
        abort_flag: Arc<RwLock<bool>>,
        thread: JoinHandle<()>,
    ) -> Self {
        AbortStream {
            stream: Box::pin(receiver.into_stream()),
            abort_flag,
            thread,
        }
    }
}

impl Stream for AbortStream {
    type Item = Result<Bytes, Error>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        match self.stream.as_mut().poll_next(cx) {
            Poll::Ready(Some(bytes)) => Poll::Ready(Some(Ok(bytes))),
            Poll::Ready(None) => {
                println!("Stream ended");
                // *self.abort_flag.write() = true;
                self.thread.abort();
                Poll::Ready(None)
            }
            Poll::Pending => Poll::Pending,
        }
    }
}
