pub mod abort_stream;

pub fn get_n_threads() -> usize {
    num_cpus::get_physical()
}
