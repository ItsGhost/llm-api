use rocket::http::Status;
use rocket::response::stream::EventStream;
use rocket::serde::json::Json;
use rocket::Either;
use rocket_db_pools::{Connection, Pool};

use crate::inference::completion::CompletionRequest;
use crate::inference::process::{start, InferenceThreadRequest};
use crate::LLMPool;

#[post("/completions", data = "<req>")]
pub async fn completions(
    mut model_pool: Connection<LLMPool>,
    req: Json<CompletionRequest>,
) -> Either<EventStream![], (Status, String)> {
    info!(
        "received completion request: {}",
        serde_json::to_string(&req.0).unwrap()
    );

    let mut model_pool = match model_pool.get().await {
        Ok(mp) => mp,
        Err(e) => return Either::Right((Status::InternalServerError, e.to_string())),
    };

    if model_pool.0.is_empty() {
        return Either::Right((
            Status::InternalServerError,
            "no llm loaded or available in the pool".to_string(),
        ));
    }

    let (token_sender, receiver) = flume::unbounded();

    let model_guard = match model_pool.get_model(req.model.as_str()) {
        Ok(mg) => mg,
        Err(e) => return Either::Right((Status::NotImplemented, e.to_string())),
    };

    let thread = start(InferenceThreadRequest {
        model_guard: model_guard.clone(),
        token_sender,
        completion_request: req.0.clone(),
    });

    Either::Left(EventStream! {
        while let Ok(resp) = receiver.recv() {
            yield resp
        }
    })
}
