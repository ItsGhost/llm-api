mod completions;

pub fn routes() -> Vec<rocket::Route> {
    routes![completions::completions]
}
