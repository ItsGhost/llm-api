#[macro_use]
extern crate rocket;

use std::str::FromStr;

use llm::Model;
use rocket::serde::Deserialize;
use rocket_db_pools::Database;

pub mod api_v1;
pub mod inference;
pub mod llmdb;
pub mod utils;

#[derive(Database)]
#[database("llm")]
pub struct LLMPool(llmdb::pool::ModelPool);

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[launch]
fn rocket() -> _ {
    let mut rocket = rocket::build();
    let figment = rocket.figment();

    rocket = rocket.attach(LLMPool::init());

    rocket
        .mount("/", routes![index])
        .mount("/v1", api_v1::routes())
}
