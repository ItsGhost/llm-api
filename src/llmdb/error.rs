use std::fmt::{Display, Formatter};

use rocket::figment;

#[derive(Debug)]
pub enum Error {
    FileNotFound,
    ModelType(String),
    LoadError(llm::LoadError),
    Config(serde_yaml::Error),
    Setup(figment::Error),
    LockError,
    NotLoaded,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:?}", self))
    }
}

impl std::error::Error for Error {}

impl From<serde_yaml::Error> for Error {
    fn from(value: serde_yaml::Error) -> Self {
        Self::Config(value)
    }
}

impl From<figment::Error> for Error {
    fn from(value: figment::Error) -> Self {
        Self::Setup(value)
    }
}

impl From<llm::LoadError> for Error {
    fn from(value: llm::LoadError) -> Self {
        Self::LoadError(value)
    }
}
