use std::path::{Path, PathBuf};

use llm::{ModelArchitecture, TokenizerSource};
use serde::Deserialize;

use crate::llmdb::error::Error;

#[derive(Deserialize, PartialEq, Clone)]
#[serde(remote = "llm::ModelParameters")]
pub struct ModelParametersDef {
    /// For [GGML formats](ggml::ContainerType) that support it, [mmap](https://en.wikipedia.org/wiki/Mmap)
    /// is the default. Although mmap typically improves performance, setting this value to `false` may
    /// be preferred in resource-constrained environments.
    #[serde(default)]
    pub prefer_mmap: bool,
    /// The context size ("memory") the model should use when evaluating a prompt. A larger context
    /// consumes more resources, but produces more consistent and coherent responses.
    #[serde(default)]
    pub context_size: usize,
    /// The [LoRA](https://arxiv.org/abs/2106.09685) adapters to use when loading the model. If `None`, no adapters will be used.
    pub lora_adapters: Option<Vec<PathBuf>>,
    /// Whether to use GPU acceleration when available
    #[serde(default)]
    pub use_gpu: bool,
}

impl Default for ModelParametersDef {
    fn default() -> Self {
        let default = llm::ModelParameters::default();
        Self {
            prefer_mmap: default.prefer_mmap,
            context_size: default.context_size,
            lora_adapters: default.lora_adapters,
            use_gpu: default.use_gpu,
        }
    }
}

#[derive(Deserialize, Clone, Default)]
pub struct ModelConfig {
    #[serde(rename = "modelType")]
    model_type: Option<String>,

    tokenizer: Option<String>,

    #[serde(rename = "defaultPromptTemplate")]
    default_prompt_template: Option<String>,

    #[serde(default, with = "ModelParametersDef")]
    pub parameters: llm::ModelParameters,
}

impl ModelConfig {
    fn determine_source(&self) -> Option<TokenizerSource> {
        if let Some(v) = &self.tokenizer {
            let path = Path::new(v.as_str());
            if path.is_absolute() && path.exists() && path.is_file() {
                return Some(TokenizerSource::HuggingFaceTokenizerFile(PathBuf::from(v)));
            } else if !v.is_empty() {
                return Some(TokenizerSource::HuggingFaceRemote(v.to_string()));
            }
        }
        None
    }

    pub fn get_tokenizer(&self) -> TokenizerSource {
        self.determine_source().unwrap_or(TokenizerSource::Embedded)
    }

    pub fn get_model_arch(&self) -> Result<Option<ModelArchitecture>, Error> {
        let default_model_type = "llama".to_string();
        let model_type = self.model_type.as_ref().unwrap_or(&default_model_type);

        let architecture: ModelArchitecture = model_type
            .parse()
            .map_err(|_| Error::ModelType(model_type.to_string()))?;

        Ok(Some(architecture))
    }
}
