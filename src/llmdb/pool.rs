use std::collections::HashMap;
use std::fs;
use std::fs::File;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use std::time::Instant;

use llm::load_progress_callback_stdout;
use rocket::figment::Figment;
use rocket::futures;
use rocket::serde::{Deserialize, Serialize};
use rocket::tokio;
use rocket::tokio::task::spawn_blocking;
use rocket_db_pools::Pool;

use crate::inference::process::ModelGuard;
use crate::llmdb::error::Error;

use super::config::ModelConfig;

pub(crate) struct ModelPoolInner {
    loaded_model_pool: Mutex<HashMap<String, ModelGuard>>,
    concurrency_count: Mutex<usize>,
    model_dir: PathBuf,
}

impl ModelPoolInner {
    pub fn new(model_dir: PathBuf) -> Self {
        Self {
            loaded_model_pool: Mutex::new(Default::default()),
            concurrency_count: Mutex::new(0),
            model_dir,
        }
    }

    pub fn is_empty(&self) -> bool {
        let mut models = self.loaded_model_pool.lock().unwrap();
        models.is_empty()
    }

    pub fn get_model(&mut self, name: &str) -> Result<ModelGuard, Error> {
        let mut pool = self
            .loaded_model_pool
            .lock()
            .map_err(|_| Error::LockError)?;
        match pool.get(name) {
            Some(m) => Ok(m.clone()),
            None => Err(Error::NotLoaded),
        }
    }

    pub async fn load_model(&self, name: &str) -> Result<ModelGuard, Error> {
        let model_base = self.model_dir.join(name);
        let model_file = model_base.with_extension("bin");
        if !model_file.exists() {
            return Err(Error::FileNotFound);
        }
        let model_config = model_base.with_extension("yaml");
        let model_config = if model_config.exists() {
            let file = File::open(model_config).unwrap();
            serde_yaml::from_reader(file)
        } else {
            Ok(ModelConfig::default())
        }?;

        info!("Loading model {name}");

        let now = Instant::now();
        let architecture = model_config.get_model_arch()?;

        let model = spawn_blocking(move || {
            llm::load_dynamic(
                architecture,
                &model_file,
                model_config.get_tokenizer(),
                model_config.parameters.into(),
                load_progress_callback_stdout,
            )
        })
        .await
        .unwrap()?;

        info!(
            "Model fully loaded! Elapsed: {}ms",
            now.elapsed().as_millis()
        );

        Ok(Arc::new(Mutex::new(model)))
    }

    #[deprecated]
    async fn spawn_pool(
        &mut self,
        model_path: PathBuf,
        model_config: &ModelConfig,
        concurrency: usize,
        cache_dir: &PathBuf,
        use_gpu: bool,
    ) -> Result<(), Error> {
        let now = std::time::Instant::now();

        let architecture = model_config.get_model_arch()?;

        let skip_copy = concurrency == 1;
        let mut tasks = vec![];
        let original_model_path = model_path.to_path_buf();

        *self.concurrency_count.lock().unwrap() = concurrency;

        for i in 0..concurrency {
            let cache_name = format!("run_cache_{}", i);
            let cache_file_path = cache_dir.join(cache_name);
            let original_model_path = original_model_path.clone();
            let tokenizer_source = model_config.get_tokenizer();
            let params = model_config.parameters.clone();
            let task = tokio::task::spawn_blocking(move || {
                if !skip_copy {
                    fs::copy(&original_model_path, &cache_file_path)
                        .map_err(|_| "Failed to copy file".to_string())?;
                }

                let cache_path = if skip_copy {
                    original_model_path
                } else {
                    cache_file_path
                };

                match llm::load_dynamic(
                    architecture,
                    cache_path.as_path(),
                    tokenizer_source,
                    params.into(),
                    load_progress_callback_stdout,
                ) {
                    Ok(model) => Ok(Arc::new(Mutex::new(Some(model)))),
                    Err(e) => Err(format!("Failed to load model: {}", e)),
                }
            });

            tasks.push(task);
        }

        let task_results = futures::future::join_all(tasks).await;

        let models: Result<Vec<_>, _> = task_results
            .into_iter()
            .map(|res| {
                res.map_err(|e| format!("{}", e))
                    .and_then(|inner| inner.map(Some))
            })
            .collect();

        // let models = models?;

        println!(
            "Model fully loaded! Elapsed: {}ms",
            now.elapsed().as_millis()
        );

        // *self.loaded_model_pool.lock().unwrap() = VecDeque::from(models);

        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
#[serde(crate = "rocket::serde")]
pub struct ModelPoolConfig {
    pub model_path: String,
    pub default_models: Option<Vec<String>>,
}

#[derive(Clone)]
pub struct ModelPool(pub(crate) Arc<ModelPoolInner>);

impl ModelPool {
    pub fn get_model(&mut self, name: &str) -> Result<ModelGuard, Error> {
        let mut pool = self
            .0
            .loaded_model_pool
            .lock()
            .map_err(|_| Error::LockError)?;
        match pool.get(name) {
            Some(m) => Ok(m.clone()),
            None => Err(Error::NotLoaded),
        }
    }
}

#[rocket::async_trait]
impl Pool for ModelPool {
    type Connection = ModelPool;
    type Error = rocket_db_pools::Error<Error, std::convert::Infallible>;

    async fn init(figment: &Figment) -> Result<Self, Self::Error> {
        let config = figment.extract::<ModelPoolConfig>()?;

        let mut inner = Arc::new(ModelPoolInner::new(PathBuf::from(config.model_path)));

        match config.default_models {
            None => {}
            Some(models) => {
                for modelName in models {
                    let model = inner.load_model(modelName.as_str()).await.unwrap();
                    inner
                        .loaded_model_pool
                        .lock()
                        .unwrap()
                        .insert(modelName, model);
                }
            }
        };

        Ok(Self(inner))
    }

    async fn get(&self) -> Result<Self::Connection, Self::Error> {
        Ok(self.clone())
    }

    async fn close(&self) {
        todo!()
    }
}
